#/usr/bin/python

#You need Python 3.6+ to use

#Install instructions:
# python3 -m pip install pyppeteer
# pyppeteer-install (This will download chromium)

#Run script:
# python3 html2pdf.py
# It will convert all html files from current directory to pdf files
# Make sure assets(.js, .css, images) are accesible from the current directory

from pyppeteer import launch
import os
import asyncio
import shlex, subprocess

async def gen_pdf(page,file):
  await page.goto('http://localhost:8000/'+file)
  await page.pdf({
    'path':file.replace('.html','.pdf'),
    'printBackground':True, 
    #'margin':{
    #  'top':'0.4in','bottom':'0.45in','left':'0.45in','right':'0.45in'
    #}, 
    #'scale':1.05,
    'format':'A4'
  })

async def convert_files(files):
  #Creating the browser instance, tries to use the current chrome
  browser = await launch({
    'headless':True,
    #Uncomment next line and edit path to use installed chrome
    #'executablePath':'/usr/bin/google-chrome-stable',
    'args':['--no-sandbox','--disable-setuid-sandbox']
  })
  #Creates a new page to load
  page = await browser.newPage()

  for file in files:
    try:
      await gen_pdf(page,file)
    except:#TODO: Catch especific Exceptions 
      raise
  #Closing page and browser
  await page.close()
  await browser.close()

#As of 2019-05-09:
#Bug causes pyppeteer to lose connection after 20s
#Future versions of pyppeteer will not require this
def patch_pyppeteer():
  import pyppeteer.connection
  original_method = pyppeteer.connection.websockets.client.connect
  def new_method(*args, **kwargs):
    kwargs['ping_interval'] = None
    kwargs['ping_timeout'] = None
    return original_method(*args, **kwargs)
  pyppeteer.connection.websockets.client.connect = new_method

def main():
  #This line should be removed in future versions!
  patch_pyppeteer()
  #Getting all files ending in .html
  html_files = [f for f in os.listdir() if f.endswith('.html')]
  #Starts a local http server
  with subprocess.Popen(shlex.split('python -m http.server')) as httpd:
    asyncio.get_event_loop().run_until_complete(convert_files(html_files))
    httpd.terminate()

if __name__ == '__main__':
  main()